/***************************************************************
 * Name:      soundtestMain.h
 * Purpose:   Defines Application Frame
 * Author:    Serg Stetsuk (serg_stetsuk@ukr.net)
 * Created:   2012-12-17
 * Copyright: Serg Stetsuk ()
 * License:
 **************************************************************/

#ifndef SOUNDTESTMAIN_H
#define SOUNDTESTMAIN_H

//(*Headers(soundtestDialog)
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/timer.h>
//*)
#include <wx/mediactrl.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>
#include <wx/tokenzr.h>
#include <wx/dynlib.h>

class soundtestDialog: public wxDialog
{
    public:

        soundtestDialog(wxWindow* parent,wxWindowID id = -1);
        virtual ~soundtestDialog();

    private:

        //(*Handlers(soundtestDialog)
        void OnQuit(wxCommandEvent& event);
        void OnLoad(wxCommandEvent& event);
        void OnStop(wxCommandEvent& event);
        void OnPlay(wxCommandEvent& event);
        void OnCustom1Paint(wxPaintEvent& event);
        void OnButton1Click(wxCommandEvent& event);
        void OnButton2Click(wxCommandEvent& event);
        void OnButton3Click(wxCommandEvent& event);
        void OnButton4Click(wxCommandEvent& event);
        void OnButton5Click(wxCommandEvent& event);
        void OnTimer1Trigger(wxTimerEvent& event);
        //*)

        //(*Identifiers(soundtestDialog)
        static const long ID_TEXTCTRL1;
        static const long ID_BUTTONPLAY;
        static const long ID_BUTTONQUIT;
        static const long ID_TIMER1;
        //*)
        static const long ID_MEDIACTRL;

        //(*Declarations(soundtestDialog)
        wxBoxSizer* BoxSizer2;
        wxButton* ButtonQuit;
        wxBoxSizer* BoxSizer1;
        wxTextCtrl* TextCtrl1;
        wxButton* ButtonPlay;
        wxTimer Timer1;
        //*)

        wxMediaCtrl* MediaCtrl;
        wxFileConfig* FileConfig;
        long CurrentState;
        long CurrentTask;
        wxArrayString TaskArray;
        wxString PrevVal;
       	wxDynamicLibrary plugin;
        typedef int (*ScanFunction_type)(int*, char*);
        ScanFunction_type ScanFunction;
//        int count;

        DECLARE_EVENT_TABLE()
};

#endif // SOUNDTESTMAIN_H
