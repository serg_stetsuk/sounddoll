/***************************************************************
 * Name:      soundtestMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    Serg Stetsuk (serg_stetsuk@ukr.net)
 * Created:   2012-12-17
 * Copyright: Serg Stetsuk ()
 * License:
 **************************************************************/

#include "soundtestMain.h"
#include <wx/msgdlg.h>
//#include <wx/sound.h>

//(*InternalHeaders(soundtestDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(soundtestDialog)
const long soundtestDialog::ID_TEXTCTRL1 = wxNewId();
const long soundtestDialog::ID_BUTTONPLAY = wxNewId();
const long soundtestDialog::ID_BUTTONQUIT = wxNewId();
const long soundtestDialog::ID_TIMER1 = wxNewId();
//*)
const long soundtestDialog::ID_MEDIACTRL = wxNewId();

BEGIN_EVENT_TABLE(soundtestDialog,wxDialog)
    //(*EventTable(soundtestDialog)
    //*)
END_EVENT_TABLE()

soundtestDialog::soundtestDialog(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(soundtestDialog)
    Create(parent, id, _("ST: 0"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
    SetClientSize(wxSize(530,136));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    BoxSizer2 = new wxBoxSizer(wxVERTICAL);
    TextCtrl1 = new wxTextCtrl(this, ID_TEXTCTRL1, _("0000"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    BoxSizer2->Add(TextCtrl1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    ButtonPlay = new wxButton(this, ID_BUTTONPLAY, _("Play"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTONPLAY"));
    BoxSizer2->Add(ButtonPlay, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    ButtonQuit = new wxButton(this, ID_BUTTONQUIT, _("Quit"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTONQUIT"));
    BoxSizer2->Add(ButtonQuit, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    BoxSizer1->Add(BoxSizer2, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    SetSizer(BoxSizer1);
    Timer1.SetOwner(this, ID_TIMER1);
    Timer1.Start(200, false);
    BoxSizer1->SetSizeHints(this);

    Connect(ID_BUTTONPLAY,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&soundtestDialog::OnPlay);
    Connect(ID_BUTTONQUIT,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&soundtestDialog::OnQuit);
    Connect(ID_TIMER1,wxEVT_TIMER,(wxObjectEventFunction)&soundtestDialog::OnTimer1Trigger);
    //*)

    MediaCtrl = new wxMediaCtrl(this, ID_MEDIACTRL);
    wxFileStream IniFile("task.ini");
    FileConfig = new wxFileConfig(IniFile);
    CurrentState = 0;
    PrevVal = "0000";
    ScanFunction = NULL;
    //count=0;

    Connect(ID_MEDIACTRL,wxEVT_MEDIA_LOADED,(wxObjectEventFunction)&soundtestDialog::OnLoad);
    Connect(ID_MEDIACTRL,wxEVT_MEDIA_STOP,(wxObjectEventFunction)&soundtestDialog::OnStop);

	plugin.Load(wxGetCwd()+_("/")+wxDynamicLibrary::CanonicalizeName(_("k2key")));
	if(plugin.IsLoaded())
	{
        wxMessageBox("LOADED!");
        ScanFunction = (ScanFunction_type) plugin.GetSymbol("K2KeyRead");
	}

}

soundtestDialog::~soundtestDialog()
{
    //(*Destroy(soundtestDialog)
    //*)
}

void soundtestDialog::OnQuit(wxCommandEvent& event)
{
//    wxMessageBox(wxString::Format("%i",count));
    Close();
}

void soundtestDialog::OnLoad(wxCommandEvent& event)
{
    MediaCtrl->Play();
}

void soundtestDialog::OnStop(wxCommandEvent& event)
{
    if(TaskArray.Count() > CurrentTask)
    {
       if (TaskArray[CurrentTask].Matches("Play(\"*\")"))
       {
            //wxMessageBox(TaskArray[CurrentTask].Mid(6,TaskArray[CurrentTask].Len()-8));
            MediaCtrl->Load(TaskArray[CurrentTask].Mid(6,TaskArray[CurrentTask].Len()-8));
       }
       else if (TaskArray[CurrentTask].Matches("Next(*)"))
       {
           //wxMessageBox(TaskArray[CurrentTask].Mid(5,TaskArray[CurrentTask].Len()-6));
           if(!TaskArray[CurrentTask].Mid(5,TaskArray[CurrentTask].Len()-6).ToLong(&CurrentState))
           {
                /*error converting*/
           }
       }
       CurrentTask++;
    }
           SetTitle("ST: " + wxString::Format("%i",CurrentState));
}

void soundtestDialog::OnPlay(wxCommandEvent& event)
{
   wxString IniResult;
   wxString IniSearch = _("state ") + wxString::Format(wxT("%i"),CurrentState) + _("/") + TextCtrl1->GetValue();
   IniResult = FileConfig->Read(IniSearch, "");
   //wxMessageBox(IniResult);
   TaskArray = wxStringTokenize(IniResult,",");
   CurrentTask = 0;
   OnStop(event);
}

void soundtestDialog::OnTimer1Trigger(wxTimerEvent& event)
{
    int i = 33;
    char str[33];
    str[0] = 1;
    for (int x = 1; x<33; x++) { str[x] = 0;}
    if(ScanFunction != NULL)
    {
        if(!ScanFunction(&i, str))
        {
         /*Read value from str */
 //        count++;
        }
    }
/*    if (TextCtrl1->GetValue().Cmp(PrevVal) != 0)
    {
        PrevVal = TextCtrl1->GetValue();
        wxCommandEvent c_event;
        OnPlay(c_event);
    }*/
}
