/***************************************************************
 * Name:      soundtestApp.h
 * Purpose:   Defines Application Class
 * Author:    Serg Stetsuk (serg_stetsuk@ukr.net)
 * Created:   2012-12-17
 * Copyright: Serg Stetsuk ()
 * License:
 **************************************************************/

#ifndef SOUNDTESTAPP_H
#define SOUNDTESTAPP_H

#include <wx/app.h>

class soundtestApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // SOUNDTESTAPP_H
