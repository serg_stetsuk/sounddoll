/***************************************************************
 * Name:      soundtestApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Serg Stetsuk (serg_stetsuk@ukr.net)
 * Created:   2012-12-17
 * Copyright: Serg Stetsuk ()
 * License:
 **************************************************************/

#include "soundtestApp.h"

//(*AppHeaders
#include "soundtestMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(soundtestApp);

bool soundtestApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	soundtestDialog Dlg(0);
    	SetTopWindow(&Dlg);
    	Dlg.ShowModal();
    	wxsOK = false;
    }
    //*)
    return wxsOK;

}
